<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Car;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class CarController extends Controller
{
    /**
     * @Route("/car", name="car")
     */
    public function index(Request $request) {
        $car = new Car();

        $form = $this->createFormBuilder($car)
            ->add('brand', TextType::class)
            ->add('model', TextType::class)
            ->add('power', IntegerType::class)
            ->add('color', ColorType::class)
            ->add('save', SubmitType::class, array('label' => 'Create a new car'))
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()){
               $car = $form->getData();


            //    return $this->redirectToRoute('show_form');
            }

        return $this->render('car/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/show", name="show_form")
     */
    public function show() {
        
        return $this->render('car/show.html.twig', []);
    }
}
